from django.contrib import admin

from .models import Word, Known, Card, CardUser, Deck, DeckUser

admin.site.register(Word)
admin.site.register(Known)
admin.site.register(Card)
admin.site.register(CardUser)
admin.site.register(Deck)
admin.site.register(DeckUser)
