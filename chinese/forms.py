from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label='username',
                               initial='joseph', # used for testing purposes only
                               max_length=50)
    password = forms.CharField(label='password',
                               max_length=50,
                               widget=forms.PasswordInput)

class DeckForm(forms.Form):
    title = forms.CharField(label='title', max_length=50)
    text = forms.CharField(label='text',
                           initial='',
                           widget=forms.Textarea,
                           max_length=700000)
    url = forms.URLField(label='url', required=False)
