from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^deck/(?P<deck_id>[0-9]+)/$',
        views.deck, name='deck'),
    url(r'^deck/(?P<deck_id>[0-9]+)/trends/$',
        views.deck_trends, name='deck_trends'),
    url(r'^api/word/known/(?P<word_id>[0-9]+)/$',
        views.mark_word_known, name='mark_word_known'),
    url(r'^api/word/viewed/(?P<word_id>[0-9]+)/$',
        views.mark_word_viewed, name='mark_word_viewed'),
]
