$(document).ready(function () {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });

  var csrftoken = getCookie('csrftoken');

  function setConfirmOnClickEvent(element) {
    element.onclick = function () {
      element.style.display = 'none';
      $.post("/api/word/known/" + element.id + "/",
        {
          name: "Donald Duck",
          city: "Duckburg"
        },
        function(data,status){
            //alert("Data: " + data + "\nStatus: " + status);
        }
      );
    }
  }

  var confirm_buttons = document.getElementsByClassName("confirm_button");
  for(var i=0; i<confirm_buttons.length; i++) {
    var confirm_button = confirm_buttons[i];
    setConfirmOnClickEvent(confirm_button);
  }

  function setSleepOnClickEvent(element) {
    element.onclick = function () {
      element.style.display = 'none';
      $.post("/api/word/viewed/" + element.id + "/",
        {
          name: "Donald Duck",
          city: "Duckburg"
        },
        function(data,status){
            //alert("Data: " + data + "\nStatus: " + status);
        }
      );
    }
  }

  var sleep_buttons = document.getElementsByClassName("sleep_button");
  for(var i=0; i<sleep_buttons.length; i++) {
    var sleep_button = sleep_buttons[i];
    setSleepOnClickEvent(sleep_button);
  }

    /*
    {% for word in words %}$
    document.getElementById("confirm_button_{{ word.id }}").onclick = function () {$
    };$
    {% endfor %}$
$
  var csrftoken = getCookie('csrftoken');$
    {% for word in words %}$
    document.getElementById("sleep_button_{{ word.id }}").onclick = function () {$
      //document.getElementById("{{ word.id }}").style.display = 'none';$
      $.post("{% url 'knowledge_sleep' word_id=word.id deck_id=deck.id %}",+$
        {$
          name: "Donald Duck",$
          city: "Duckburg"$
        },$
        function(data,status){$
            //alert("Data: " + data + "\nStatus: " + status);$
        });$
    };$
    {% endfor %}$
    */
}
)
