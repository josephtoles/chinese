from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from chinese.models import Deck, Word, Card, CardUser, DeckUser, Known
from collections import deque
import sys


class Command(BaseCommand):
    help = 'Uploads a corpus for vocabulary'

    def add_arguments(self, parser):
        parser.add_argument('path', nargs='+', type=str) # first arg
        parser.add_argument('title', nargs='+', type=str) # second arg

    def handle(self, *args, **options):
        user = User.objects.get(username='joseph')
        path = options['path'][0]
        title = options['title'][0]
        print('Adding ' + path + ' as ' + title)
        with open(path, 'r') as f:
            text = f.read()
            print('finished reading')
            _ = self.create_deck_from_text(text=text, title=title, url='')
        print('Deck finished uploading')

    def create_deck_from_text(self, text, title, url):
        deck = Deck.objects.create(title=title,
                                link=url,
                                num_unique_words=0, # placeholder
                                num_repeat_words=0) # placeholder

        full_text = title + text
        num_unique_words = 0
        num_repeat_words = 0

        token_set = {w.simplified for w in Word.objects.all()}
        vocab_trie = VocabTrie(token_set)

        text_length = len(full_text)
        text_length_length = "{0}".format(len("{0}".format(text_length)))
        format_string = '\rindex {0:' + text_length_length + '} of {1}'

        index = 0
        rank = 0
        while index < text_length:
            sys.stdout.write(format_string.format(index + 1, text_length))
            sys.stdout.flush()
            found_word = False
            word_length = 20 # 20 is longest string in the dictionary
            while word_length >= 1 and not found_word:
                token = text[index:index+word_length]
                if vocab_trie.contains_word(token):
                    word = Word.objects.get(simplified=token)
                    num_repeat_words += 1
                    try:
                        card = Card.objects.get(word=word, deck=deck)
                        card.count += 1
                        card.save()
                    except Card.DoesNotExist:
                        card = Card.objects.create(word=word,
                                                deck=deck,
                                                count=1,
                                                rank=rank)
                        rank += 1
                        for user in User.objects.all():
                            known = Known.objects.filter(user=user, word=word).first()
                            CardUser.objects.create(user=user,
                                                    card=card,
                                                    last_viewed=None,
                                                    word=word,
                                                    deck=deck,
                                                    known=known)
                        num_unique_words += 1
                    found_word = True
                if not found_word:
                    word_length -= 1
            if found_word:
                index += word_length
            else:
                index += 1
        sys.stdout.write('\n')

        deck.num_unique_words = num_unique_words
        deck.num_repeat_words = num_repeat_words
        deck.save()

        for user in User.objects.all():
            deck_user = DeckUser.objects.create(user=user,
                                                deck=deck,
                                                num_known_unique_words=0,
                                                num_known_repeat_words=0)
            deck_user.set_optimizations()

        return deck


class CharacterNode():
    def __init__(self, character):
        self.character = character
        self.final = False
        self.children = {}

    def set_final(self):
        self.final = True
        

class VocabTrie():
    def __init__(self, words=set()):
        self.root = CharacterNode(character="")
        for word in words:
            self.add_word(word)
        return
        
    def contains_word(self, word):
        chars = deque(c for c in word)
        next_node = self.root
        while chars:
            c = chars.popleft()
            if c in next_node.children:
                next_node = next_node.children[c]
            else:
                return False
        if next_node.final:
            return True
        else:
            return False

    def add_word(self, word):
        chars = deque(c for c in word)
        next_node = self.root
        while chars:
            c = chars.popleft()
            if c in next_node.children:
                next_node = next_node.children[c]
            else:
                new_child = CharacterNode(c)
                next_node.children[c] = new_child
                next_node = new_child
            # last char? indicate terminus
            if not chars:
                next_node.set_final()
