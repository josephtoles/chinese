import os
import sys
from django.core.management.base import BaseCommand, CommandError

from chinese.models import Knowledge

class Command(BaseCommand):
    help = 'use this to load in cedict'

    def handle(self, *args, **options):
        known = Knowledge.objects.filter(knowledge_level=Knowledge.KNOWN).all()
        for k in known:
            sys.stdout.write(k.simplified.token + '\n')
        sys.stdout.flush()
