from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from chinese.models import Deck, Word, Card, CardUser, DeckUser, Known
import sys

class Command(BaseCommand):
    help = 'Repairs some database corruption'

    def handle(self, *args, **options):
        print("Resetting deck card counts")
        i = 0
        deck_count = Deck.objects.count()
        for d in Deck.objects.all():
            i += 1
            print("triggering " + str(i) + " of " + str(deck_count))
            d.reset_optimizations()

        print("Erasing known counts")
        for du in DeckUser.objects.all():
            du.num_known_unique_words = 0
            du.num_known_repeat_words = 0
            du.save()
        print("Done erasing known counts")

        i = 0
        known_count = Known.objects.count()
        for k in Known.objects.all():
            print("triggering " + str(i) + " of " + str(known_count))
            i += 1
            k.trigger_optimizations()
        print('Safe to continue')

