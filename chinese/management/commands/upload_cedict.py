import os
import sys
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError

import chinese
from chinese.models import Word

# You can get the dictionary itself at
# https://www.mdbg.net/chinese/dictionary?page=cc-cedict

DICTIONARY_PATH = 'primer/cedict_ts.u8'

class Command(BaseCommand):
    help = 'use this to load in cedict'

    def handle(self, *args, **options):
        full_path = os.path.join(os.path.dirname(chinese.__file__),
                                 DICTIONARY_PATH)
        if os.path.exists(full_path):
            self.read_to_database(file_path=full_path)
        else:
            raise CommandError('No dictionary at {0}'.format(full_path))

    def read_to_database(self, file_path):
        line_number = 0
        num_words_added = 0
        num_words_appended = 0
        num_words_skipped = 0

        # String output
        num_lines = sum(1 for line in open(file_path))
        number_length = "{0}".format(len("{0}".format(num_lines)))
        format_string = '\rAdding word {0:' +  number_length + '} / {1}'

        with open(file_path) as f:
            for line in f:
                line_number += 1
                sys.stdout.write(format_string.format(line_number,
                                                      num_lines))
                sys.stdout.flush()
                if line.startswith('#'):
                    continue

                # traditional field of Word
                end_traditional_index = line.index(' ')
                traditional = line[:end_traditional_index]
                line = line[end_traditional_index + 1:]

                # simplified field of Word
                end_simplified_index = line.index(' ')
                simplified = line[:end_simplified_index]
                line = line[end_simplified_index+1:]

                # pinyin field of Word
                begin_pinyin_index = 1
                end_pinyin_index = line.index(']')
                pinyin = line[begin_pinyin_index:end_pinyin_index]
                line = line[end_pinyin_index + 1:]

                # definition field of Word
                definition = line[1:len(line) - 1] # removes trailing return
 
                try:
                    word = Word.objects.get(simplified=simplified)
                    if pinyin in word.pinyin and definition in word.definition:
                        num_words_skipped += 1
                    else:
                        # throws away any potential differen traditionals
                        word.pinyin = '{0}\n{1}'.format(word.pinyin,
                                                        pinyin)
                        word.definition = '{0}\n{1}'.format(word.definition,
                                                            definition)
                        word.definition_count += 1
                        word.save()
                        num_words_appended += 1
                except Word.DoesNotExist:
                    Word.objects.create(simplified=simplified,
                                        traditional=traditional,
                                        pinyin=pinyin,
                                        definition=definition)
                    num_words_added += 1


        print('\nAdded {0} words. Appended {3}. Skipped {4}.',
              num_words_added,
              num_words_appended,
              num_words_skipped)
                
