from django.utils import timezone
from datetime import datetime, timedelta
from django.contrib.auth import authenticate
from django.contrib.auth import login as login_user, logout as logout_user
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from .forms import LoginForm, DeckForm
from .models import Card, Known, Deck, Word, CardUser, DeckUser

# Homepage

def index(request):
    if request.user.is_authenticated():
        decks = Deck.objects.all()
        deck_users = DeckUser.objects.filter(user=request.user).all()
        sorted_deck_users = sorted(deck_users, key=lambda du: du.num_unknown_unique_words())
        return render(request,
                      'chinese/me.html',
                      {
                            'decks': decks,
                            'deck_users': sorted_deck_users,
                      })
    else:
        form = DeckForm(request.POST)
        return render(request, 'chinese/index.html', {'form': form})

# User authentication

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'],
                                password=form.cleaned_data['password'])
            if user is not None:
                login_user(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponseRedirect(reverse('login'))
    else:
        form = LoginForm()
        return render(request, 'chinese/login.html', {'form': form})

def logout(request):
    logout_user(request)
    return HttpResponseRedirect(reverse('index'))
    
# Deck views

def get_deck_context(request, deck_id):
    if request.user.is_authenticated():
        deck = Deck.objects.get(id=deck_id)
        context = {'deck': deck, 'words': deck.get_words(request.user)}
        return context
    else:
        return None

def deck(request, deck_id):
    context = get_deck_context(request, deck_id)
    if context != None:
        return render(request, 'chinese/deck.html', context)
    else:
        return HttpResponseRedirect('/login/')

def deck_trends(request, deck_id):
    if request.user.is_authenticated():
        deck = get_object_or_404(Deck, id=deck_id)
        deck_user = DeckUser.objects.get(user=request.user, deck=deck)

        # current numbers
        #unique_words = deck.num_unique_words
        #repeat_words = deck.num_repeat_words
        #unknown_unique_words = deck_user.num_unknown_unique_words
        #unknown_unique_words = deck_user.num_unknown_repeat_words

        def unique_repeat_days_ago(days):
            x_days_ago = timezone.now() - timedelta(days=days)
            card_users_last_x_days = CardUser.objects.order_by('known__created').filter(known__created__gte=x_days_ago, deck=deck).all()
            average = "{0:.2f}".format((1.0 * len(card_users_last_x_days)) / days)
            output = (len(card_users_last_x_days), average, sum(cu.card.count for cu in card_users_last_x_days))
            return output
        
        unique_words_last_day, average_last_day, repeat_words_last_day = unique_repeat_days_ago(days=1)
        unique_words_last_2_days, average_last_2_days, repeat_words_last_2_days = unique_repeat_days_ago(days=2)
        unique_words_last_7_days, average_last_7_days, repeat_words_last_7_days = unique_repeat_days_ago(days=7)
        unique_words_last_30_days, average_last_30_days, repeat_words_last_30_days = unique_repeat_days_ago(days=30)

        context = {
            'deck': deck,
            'deck_user': deck_user,

            'unique_total': deck.num_unique_words,
            'unknown_unique_words': deck_user.num_unknown_unique_words,
            'repeat_total': deck.num_repeat_words,
            'unknown_repeat_words': deck_user.num_unknown_repeat_words,
            
            'unique_words_last_day': unique_words_last_day,
            'average_last_day': average_last_day,
            'repeat_words_last_day': repeat_words_last_day,
            
            'unique_words_last_2_days': unique_words_last_2_days,
            'average_last_2_days': average_last_2_days,
            'repeat_words_last_2_days': repeat_words_last_2_days,
            
            'unique_words_last_7_days': unique_words_last_7_days,
            'average_last_7_days': average_last_7_days,
            'repeat_words_last_7_days': repeat_words_last_7_days,

            'unique_words_last_30_days': unique_words_last_30_days,
            'average_last_30_days': average_last_30_days,
            'repeat_words_last_30_days': repeat_words_last_30_days,
        }
        return render(request, 'chinese/deck_trends.html', context)
    else:
        return HttpResponseRedirect('/login/')

# API used to mark place.
def mark_word_viewed(request, word_id):
    word = Word.objects.get(id=word_id)
    if request.user.is_authenticated():
        if request.method == 'POST':
            card_user = CardUser.objects.get(user=request.user, word=word)
            card_user.last_viewed = datetime.now()
            card_user.save()
            return HttpResponse("post request success")
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseForbidden()

# API used to mark place.
def mark_word_known(request, word_id):
    word = Word.objects.get(id=word_id)
    if request.user.is_authenticated():
        if request.method == 'POST':
            known = Known.objects.create(user=request.user, word=word)
            known.trigger_optimizations()
            return HttpResponse("post request success")
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseForbidden()
        
