from django.conf import settings
from django.db import models
from django.utils import timezone
from django.db.models import Sum


class Word(models.Model):
    simplified = models.CharField(max_length=20, db_index=True, unique=True)
    traditional = models.CharField(max_length=40)
    pinyin = models.TextField() # carriage-return-separated list
    definition = models.TextField() # carriage-return-separated list
    definition_count = models.IntegerField(default=1)

    def pinyin_with_breaks(self):
        return self.pinyin.replace('/', '<br>')

    def definition_with_breaks(self):
        # The 1 and -1 indices are to remove the starting and trailing slashes
        return self.definition[1:-1].replace('/', '<br>')

    def __str__(self):
        return self.simplified


class Known(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, db_index=True)
    word = models.ForeignKey('Word', db_index=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{0} {1}'.format(self.user.username, str(self.word))

    class Meta:
        unique_together = (('user', 'word'),)
        ordering = (('created'),)

    # call this method after creating a Known object
    def trigger_optimizations(self):
        card_users = CardUser.objects.filter(user=self.user, word=self.word)
        for card_user in card_users:
            card_user.known = self
            card_user.save()
            deck_user, _ = DeckUser.objects.get_or_create(deck=card_user.deck,
                                                          user=self.user)
            deck_user.num_known_unique_words += 1
            deck_user.num_known_repeat_words += card_user.card.count
            deck_user.save()


class Card(models.Model):
    word = models.ForeignKey('Word', db_index=True)
    deck = models.ForeignKey('Deck', db_index=True)
    count = models.IntegerField(db_index=True)
    rank = models.IntegerField(db_index=True)

    def __str__(self):
        return 'Card {0}'.format(str(self.id))

    class Meta:
        unique_together = (('deck', 'word'),)
        ordering = (('count'), ('rank'),)


class CardUser(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, db_index=True)
    card = models.ForeignKey('Card', db_index=True)
    last_viewed = models.DateTimeField(null=True, default=None, db_index=True)

    # optimizations
    word = models.ForeignKey('Word', db_index=True)
    deck = models.ForeignKey('Deck', db_index=True)
    known = models.ForeignKey('Known', db_index=True, null=True)

    def __str__(self):
        return 'Card {0} {1}'.format(str(self.id), self.user.username)

    class Meta:
        unique_together = (('user', 'card'),)
        ordering = (('-card__count'), ('card__rank'),)


class Deck(models.Model):
    #cards = models.ManyToManyField('Word', through='Card')
    title = models.TextField(blank=False)
    link = models.URLField(blank=True)

    # optimizations
    num_unique_words = models.IntegerField(db_index=True) # ∑ unique*1
    num_repeat_words = models.IntegerField(db_index=True) # ∑ unique*frequency

    def reset_optimizations(self):
        cards = Card.objects.filter(deck=self)
        self.num_unique_words = len(cards)
        self.num_repeat_words = 0
        for card in cards:
            self.num_repeat_words += card.count
            print(card.count)
        self.save()

    def get_words(self, user):
        card_users = CardUser.objects.filter(deck=self, user=user, known=None)
        return [card_user.word for card_user in card_users]

    def __str__(self):
        return self.title


class DeckUser(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, db_index=True)
    deck = models.ForeignKey('Deck', db_index=True)

    # optimizations
    num_known_unique_words = models.IntegerField(db_index=True, default=0)
    num_known_repeat_words = models.IntegerField(db_index=True, default=0)

    def num_unknown_unique_words(self):
        return self.deck.num_unique_words - self.num_known_unique_words

    def num_unknown_repeat_words(self):
        return self.deck.num_repeat_words - self.num_known_repeat_words

    def completed(self):
        return self.num_known_repeat_words >= self.deck.num_repeat_words

    def percent_string(self):
        if self.completed():
            return "100"
        if (self.deck.num_repeat_words == 0):
            return "100"
        return "{0:.1f}".format(100.0 * self.num_known_repeat_words / self.deck.num_repeat_words)

    class Meta:
        unique_together = (('user', 'deck'),)
        ordering = (('deck__num_unique_words'),)

    def set_optimizations(self):
        num_unknown_unique_words = CardUser.objects.filter(deck=self.deck,
                                                           user=self.user,
                                                           known=None).count()
        self.num_known_unique_words = self.deck.num_unique_words - num_unknown_unique_words

        aggregate = CardUser.objects.filter(deck=self.deck,
                                            user=self.user,
                                            known=None).aggregate(repeat_count=Sum('card__count'))
        num_unknown_repeat_words = aggregate['repeat_count']
        if num_unknown_repeat_words == None:
            num_unknown_repeat_words = 0
        self.num_known_repeat_words = self.deck.num_repeat_words - num_unknown_repeat_words
        self.save()

