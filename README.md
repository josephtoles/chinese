#+TITLE: Instructions for my personal website$

* Setup$
** Azure server prerequisites
   sudo apt-get install tdsodbc
** Azure server prerequisies [doesn't fix anything]
   Instructions available under "Ubuntu 26.4" at
   https://docs.microsoft.com/en-us/sql/connect/odbc/linux/installing-the-microsoft-odbc-driver-for-sql-server-on-linux
   These are the instructions
   ```
    sudo su 
    curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
    curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
    exit
    sudo apt-get update
    sudo ACCEPT_EULA=Y apt-get install msodbcsql=13.1.4.0-1 mssql-tools-14.0.3.0-1 unixodbc-dev
    echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
    echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
    source ~/.bashrc
   ```
** Python library prerequisites
*** Python pip database prerequisites
    This is a prerequisite for deploying on azure
    $ sudo apt-get install unixodbc unixodbc-dev

** Python setup

To install virtualenv
$ sudo apt-get install python-virtualenv

To setup virtualenv
$ virtualenv -p python3 env

To activate virtualenv
$ source /env/bin/activate

To deactivate virtualenv
$ deactivate

To install requirements
$ pip3 install -r requirements.txt


* Remove non UTF-8 Characters with
  iconv -f utf-8 -t utf-8 -c file.txt > file-cleaned.txt

* Runserver
Login as superuser with
`$ sudo -s`

Then run the server with
`./manage.py runserver 0.0.0.0:80`

